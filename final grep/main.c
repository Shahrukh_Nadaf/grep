#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include "grep.h"
int main(int argc, char *argv[]) {
	FILE *fp;
	data d;
	int option, iflag = 0, vflag = 0, wflag = 0, eflag = 0, cflag = 0, mflag = 0, Hflag = 0, hflag = 0, rflag = 0, fflag = 0, qflag = 0, bflag = 0, mnum, ei = 0, n = -1, fi = 0;
	char epattern[32][32], ffile[32], word[32], file[32];
	DIR *dir;
	struct dirent *sd;
	files f;
	while((option = getopt(argc, argv, "ivwe:cm:Hhrf:qb")) != -1) {		//sets flag to 1 for whichever options are given
		switch(option) {
			case 'i' :
				iflag = 1;
				break;
			case 'v' :
				vflag = 1;
				break;
			case 'w' :
				wflag = 1;
				break;
			case 'e' :
				eflag = 1;
				strcpy(epattern[ei++], optarg);
				break;
			case 'c' :
				cflag = 1;
				break;
			case 'm' :
				mflag = 1;
				mnum = atoi(optarg);
				break;
			case 'H' :
				Hflag = 1;
				break;
			case 'h' :
				hflag = 1;
				break;
			case 'r' :
				rflag = 1;
				Hflag = 1;
				break;
			case 'f' :
				fflag = 1;
				strcpy(ffile, optarg);
				break;
			case 'q' :
				qflag = 1;
				break;
			case 'b' :
				bflag = 1;
				break;
			default :
				printf("Invalid options!\n");
				return -1;
		}
	}
	if(iflag == 0 && wflag == 0)
		n = 1;
	if(iflag == 0 && wflag == 1)
		n = 2;
	if(iflag == 1 && wflag == 0)
		n = 3;
	if(iflag == 1 && wflag == 1)
		n = 4;
	if(vflag)
		n = -1;	
	if(rflag) {
		f.fi = 0;
		allfiles(&f, ".");
		strcpy(word, argv[argc-1]);
	}	
	else {
		f.fi = 0;
		strcpy(word, argv[argc-2]);
		strcpy(f.folder[(f.fi)], ".");
		strcpy(f.file[(f.fi)++], argv[argc-1]);
	}
	int j = 0;
	while(j < f.fi) {
//		printf("f.folder[%d] = %s, f.file[%d] = %s\n", j, f.folder[j], j, f.file[j]);
		if(strcmp(f.folder[j], ".") != 0) {
			strcpy(file, f.folder[j]);
			strcat(file, "/");
			strcat(file, f.file[j]);
			strcpy(f.file[j], file);
		}
		fp = fopen(f.file[j], "r");
		if(fp == NULL) {
			printf("Error opening file '%s'\n", f.file[j]);
			return -1;
		}
		storeData(fp, &d);
		if(fflag == 0 && eflag == 0)
			find(&d, word, iflag, wflag, mflag, mnum);
		if(eflag) {
			int i = 0;
			while(i < ei) {
				find(&d, epattern[i], iflag, wflag, mflag, mnum);
				i++;
			}
		}
		if(fflag) {
			FILE *fp1 = fopen(ffile, "r");
			if(fp1 == NULL) {
				printf("Error opening file '%s'\n", ffile);
				return -1;
			}
			char fpattern[16];
			while(fgets(fpattern, 16, fp1) != NULL) {
				strtok(fpattern, "\n");
				find(&d, fpattern, iflag, wflag, mflag, mnum);
			}
		}

		print(&d, n, cflag, Hflag, f.file[j], hflag, qflag, bflag, mflag, mnum);
		j++;
	}
	fcloseall();
	return 0;
}
