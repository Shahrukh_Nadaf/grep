#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include "grep.h"
#define RED "\x1B[31m"
#define GREEN "\x1B[32m"
#define CYN "\x1B[36m"
#define PURPLE "\x1B[35m"
#define RESET "\x1B[0m"
void init(data *d) {
	d->head = d->tail = NULL;
}
void addNode(data *d, char *line) {  				//adds a node with given line to list
	node *tmp = (node *)malloc(sizeof(node));
	tmp->line = (char *)malloc(sizeof(char) * 1024);
	strcpy(tmp->line, line);
	tmp->n = -1;
	tmp->byte = 0;
	tmp->next = tmp->prev = NULL;
	if(d->head == NULL && d->tail == NULL) 
		d->head = d->tail = tmp;
	else {
		d->tail->next = tmp;
		tmp->prev = d->tail;
		d->tail = tmp;
	}
}
void storeData(FILE *fp, data *d) {			//stores each line in file as a node
	init(d);
	char line[1024];
	while(fgets(line, 1024, fp)) 
		addNode(d, line);
}
void printData(data *d) {				//prints whole node with all info in it 
	node *tmp = d->head;
	while(tmp) {
		printf("%s", tmp->line);
		printf("%d\n", tmp->n);
		tmp = tmp->next;
	}
}
							//takes node. Finds given searchstring in it. If found sets n to proper value 
void find(data *d, char *word, int iflag, int wflag, int mflag, int mnum) {
	if(iflag == 0 && wflag == 0 && mflag == 0) {
		if(strstr(word, ".") && strstr(word, "*")) {
			char *token1, *token2, *found;
			token1 = strtok(word, ".");
			token2 = strtok(NULL, "\0");
			token2++;
			node *tmp = d->head;
			while(tmp) {
				if(tmp->prev)
					tmp->byte = tmp->prev->byte + strlen(tmp->prev->line);
				if(found = strstr(tmp->line , token1))
					if(strstr(found, token2)) 
						tmp->n = 1;
				tmp = tmp->next;
			}
		}
		else{
			node *tmp = d->head;
			while(tmp) {
				if(tmp->prev)
					tmp->byte = tmp->prev->byte + strlen(tmp->prev->line);
				if(strstr(tmp->line, word))
					tmp->n = 1;
				tmp = tmp->next;
			}
		}
	}
	if(iflag == 0 && wflag == 0 && mflag == 1) {
		if(strstr(word, ".") && strstr(word, "*")) {
			char *token1, *token2, *found;
			token1 = strtok(word, ".");
			token2 = strtok(NULL, "\0");
			token2++;
			node *tmp = d->head;
			int count = 0;
			while(tmp && (count < mnum)) {
				if(tmp->prev)
					tmp->byte = tmp->prev->byte + strlen(tmp->prev->line);
				if(found = strstr(tmp->line , token1))
					if(strstr(found, token2)) {
						tmp->n = 1;
						count++;
					}
				tmp = tmp->next;
			}
		}
		else {
			node *tmp = d->head;
			int count = 0;
			while(tmp && (count < mnum)) {
				if(tmp->prev)
					tmp->byte = tmp->prev->byte + strlen(tmp->prev->line);
				if(strstr(tmp->line, word)) {
					tmp->n = 1;
					count++;
				}
				tmp = tmp->next;
			}
		}
	}
	if(iflag == 0 && wflag == 1 && mflag == 0) {
		if(strstr(word, ".") && strstr(word, "*")) {
			char *token1, *token2, *found;
			token1 = strtok(word, ".");
			token2 = strtok(NULL, "\0");
			token2++;
			node *tmp = d->head;
			while(tmp) {
				if(tmp->prev)
					tmp->byte = tmp->prev->byte + strlen(tmp->prev->line);
				if(found = strstr(tmp->line , token1))
					if(strstr(found, token2))
						tmp->n = 2;
				tmp = tmp->next;
			}
		}	
		else {
			node *tmp = d->head;
			while(tmp) {
				char line[1048], line1[1048] = " ", word1[1048] = " ";
				strcpy(line, tmp->line);
				if(strtok(line, "\n"))
					strcat(line1, line);
				strcat(word1, word);
				strcat(line1, " ");
				strcat(word1, " ");
				if(tmp->prev)
					tmp->byte = tmp->prev->byte + strlen(tmp->prev->line);
				if(strstr(line1, word1))
					tmp->n = 2;
				tmp = tmp->next;
			}
		}
	}
	if(iflag == 0 && wflag == 1 && mflag == 1) {
		if(strstr(word, ".") && strstr(word, "*")) {
			char *token1, *token2, *found;
			token1 = strtok(word, ".");
			token2 = strtok(NULL, "\0");
			token2++;
			node *tmp = d->head;
			int count = 0;
			while(tmp && (count < mnum)) {
				if(tmp->prev)
					tmp->byte = tmp->prev->byte + strlen(tmp->prev->line);
				if(found = strstr(tmp->line , token1))
					if(strstr(found, token2)) {
						tmp->n = 2;
						count++;
					}
				tmp = tmp->next;
			}
		}
		else {
			node *tmp = d->head;
			int count = 0;	
			while(tmp && (count < mnum)) {
				char line[1024], line1[1024] = " ", word1[1024] = " ";
				strcpy(line, tmp->line);
				strcat(line1, strtok(line, "\n"));
				strcat(word1, word);
				strcat(line1, " ");
				strcat(word1, " ");
				if(tmp->prev)
					tmp->byte = tmp->prev->byte + strlen(tmp->prev->line);
				if(strstr(line1, word1)) {
					tmp->n = 2;
					count++;
				}
				tmp = tmp->next;
			}
		}
	}
	if(iflag == 1 && wflag == 0 && mflag == 0) {
		if(strstr(word, ".") && strstr(word, "*")) {
			char *token1, *token2, *found;
			token1 = strtok(word, ".");
			token2 = strtok(NULL, "\0");
			token2++;
			node *tmp = d->head;
			while(tmp) {
				if(tmp->prev)
					tmp->byte = tmp->prev->byte + strlen(tmp->prev->line);
				if(found = strcasestr(tmp->line , token1))
					if(strcasestr(found, token2))
						tmp->n = 3;
				tmp = tmp->next;
			}
		}	
		else {
			node *tmp = d->head; 
			while(tmp) {
				if(tmp->prev)
					tmp->byte = tmp->prev->byte + strlen(tmp->prev->line);
				if(strcasestr(tmp->line, word))
					tmp->n = 3;
				tmp = tmp->next;
			}
		}
	}
	if(iflag == 1 && wflag == 0 && mflag == 1) {
		if(strstr(word, ".") && strstr(word, "*")) {
			char *token1, *token2, *found;
			token1 = strtok(word, ".");
			token2 = strtok(NULL, "\0");
			token2++;
			node *tmp = d->head;
			int count = 0;
			while(tmp && (count < mnum)) {
				if(tmp->prev)
					tmp->byte = tmp->prev->byte + strlen(tmp->prev->line);
				if(found = strcasestr(tmp->line , token1))
					if(strcasestr(found, token2)) {
						tmp->n = 3;
						count++;
					}
				tmp = tmp->next;
			}
		}
		else {
			node *tmp = d->head;
			int count = 0;
			while(tmp && (count < mnum)) {
				if(tmp->prev)
					tmp->byte = tmp->prev->byte + strlen(tmp->prev->line);
				if(strcasestr(tmp->line, word)) {
					tmp->n = 3;
					count++;
				}
				tmp = tmp->next;
			}
		}
	}
	if(iflag == 1 && wflag == 1 && mflag == 0) {
		if(strstr(word, ".") && strstr(word, "*")) {
			char *token1, *token2, *found;
			token1 = strtok(word, ".");
			token2 = strtok(NULL, "\0");
			token2++;
			node *tmp = d->head;
			while(tmp) {
				if(tmp->prev)
					tmp->byte = tmp->prev->byte + strlen(tmp->prev->line);
				if(found = strcasestr(tmp->line , token1))
					if(strcasestr(found, token2))
						tmp->n = 4;
				tmp = tmp->next;
			}
		}	
		else {
			node *tmp = d->head;
			while(tmp) {
				char line[1024], line1[1024] = " ", word1[1024] = " ";
				strcpy(line, tmp->line);
				strcat(line1, strtok(line, "\n"));
				strcat(word1, word);
				strcat(line1, " ");
				strcat(word1, " ");
				if(tmp->prev)
					tmp->byte = tmp->prev->byte + strlen(tmp->prev->line);
				if(strcasestr(line1, word1))
					tmp->n = 4;
				tmp = tmp->next;
			}
		}
	}
	if(iflag == 1 && wflag == 1 && mflag == 1) {
		if(strstr(word, ".") && strstr(word, "*")) {
			char *token1, *token2, *found;
			token1 = strtok(word, ".");
			token2 = strtok(NULL, "\0");
			token2++;
			node *tmp = d->head;
			int count = 0;
			while(tmp && (count < mnum)) {
				if(tmp->prev)
					tmp->byte = tmp->prev->byte + strlen(tmp->prev->line);
				if(found = strcasestr(tmp->line , token1))
					if(strcasestr(found, token2)) {
						tmp->n = 4;
						count++;
					}
				tmp = tmp->next;
			}
		}
		else {
			node *tmp = d->head;
			int count = 0;	
			while(tmp && (count < mnum)) {
				char line[1024], line1[1024] = " ", word1[1024] = " ";
				strcpy(line, tmp->line);
				strcat(line1, tmp->line);
				strcat(word1, word);
				strcat(line1, " ");
				strcat(word1, " ");
				if(tmp->prev)
					tmp->byte = tmp->prev->byte + strlen(tmp->prev->line);
				if(strcasestr(line1, word1)) {
					tmp->n = 4;
					count++;
				}
				tmp = tmp->next;
			}
		}
	}
}
				//prints all lines found matching with given searchstring
void print(data *d, int n, int cflag, int Hflag, char *file, int hflag, int qflag, int bflag, int mflag, int mnum) {
	node *tmp;
	if(qflag) {
		tmp = d->head;
		while(tmp) {
			if(tmp->n == n)
				exit(1);
			tmp = tmp->next;
		}
	}
	else {
		tmp = d->head;
		if(hflag || (hflag == 0 && Hflag == 0)) {
			if(cflag == 0 && bflag == 0) {
				while(tmp) {
					if(tmp->n == n)
						printf("%s", tmp->line);
				tmp = tmp->next;
				}
			}
			if(cflag == 0 && bflag == 1) {
				while(tmp) {
					if(tmp->n == n) 
						printf(GREEN"%d"RESET CYN":"RESET"%s", tmp->byte, tmp->line);
				tmp = tmp->next;
				}
			}
			if(cflag == 1 && bflag == 0) {
				int count = 0;
				while(tmp) {
					if(tmp->n == n) 
						count++;
				tmp = tmp->next;
				}
				if(mflag) {
					if(count <= mnum)		
						printf("%d\n", count);
					else
						printf("%d\n", mnum);
				}
				else
					printf("%d\n", count);
			}
			if(cflag== 1 && bflag == 1) {
				int count = 0;
				while(tmp) {
					if(tmp->n == n) 
						count++;
				tmp = tmp->next;
				}
				if(mflag) {
					if(count <= mnum)		
						printf("%d\n", count);
					else
						printf("%d\n", mnum);
				}
				else
					printf("%d\n", count);
			}
		}
		else if(Hflag) {
			if(cflag == 0 && bflag == 0) {
				while(tmp) {
					if(tmp->n == n)
						printf(PURPLE"%s"RESET CYN":"RESET"%s", file, tmp->line);
				tmp = tmp->next;
				}
			}
			if(cflag == 0 && bflag == 1) {
				while(tmp) {
					if(tmp->n == n)
						printf(PURPLE"%s"RESET CYN":"RESET GREEN"%d"RESET CYN":"RESET"%s", file, tmp->byte, tmp->line);
				tmp = tmp->next;
				}
			}
			if(cflag == 1 && bflag == 0) {
				int count = 0;
				while(tmp) {
					if(tmp->n == n) 
						count++;
				tmp = tmp->next;
				}
				if(mflag) {
					if(count <= mnum)		
						printf(PURPLE"%s"RESET CYN":"RESET"%d\n", file, count);
					else
						printf(PURPLE"%s"RESET CYN":"RESET"%d\n", file, mnum);
				}
				else
					printf(PURPLE"%s"RESET CYN":"RESET"%d\n", file, count);
			}
			if(cflag== 1 && bflag == 1) {
				int count = 0;
				while(tmp) {
					if(tmp->n == n) 
						count++;
				tmp = tmp->next;
				}
				if(mflag) {
					if(count <= mnum)		
						printf(PURPLE"%s"RESET CYN":"RESET"%d\n", file, count);
					else
						printf(PURPLE"%s"RESET CYN":"RESET"%d\n", file, mnum);
				}
				else
					printf(PURPLE"%s"RESET CYN":"RESET"%d\n", file, count);
			}
		}
	}
}
void allfiles(files *f, char *folder) {			//given a folder stores each file and its corresponding folder path in structure
	char fol[128];
	DIR *dir;
	struct dirent *sd, *entry, **result;
	dir = opendir(folder);
	if(dir == NULL) {
		printf("Error opening directory %s!\n", folder);
		return ;
	}
	while((sd = readdir(dir)) != NULL) {
		if(strcmp(sd->d_name, ".") == 0)
			continue;
		if(strcmp(sd->d_name, "..") == 0)
			continue;
		strcpy((f->file[(f->fi)]), sd->d_name);
		strcpy((f->folder[(f->fi)]), folder);
		(f->fi)++;
		strcpy(fol, f->folder[(f->fi)-1]);
		strcat(fol, "/");
		strcat(fol, f->file[(f->fi)-1]);
		if(opendir(fol))
			allfiles(f, fol);
	}
}
